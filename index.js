const {
  Keystone
} = require('@keystonejs/keystone');
const {
  PasswordAuthStrategy
} = require('@keystonejs/auth-password');
const {
  Text,
  Checkbox,
  Password,
  DateTime,
  Select,
  Relationship,
  Integer
} = require('@keystonejs/fields');
const {
  GraphQLApp
} = require('@keystonejs/app-graphql');
const {
  AdminUIApp
} = require('@keystonejs/app-admin-ui');
const initialiseData = require('./initial-data');

const {
  MongooseAdapter: Adapter
} = require('@keystonejs/adapter-mongoose');
const PROJECT_NAME = 'kyrgyzHorse';
const adapterConfig = {
  mongoUri: 'mongodb://superuser:z4-qwVF%2F%5EG7%7DT%24K%5B@rdfmap.kg/kyrgyz-horse?authSource=admin'
};


const keystone = new Keystone({
  adapter: new Adapter(adapterConfig),
  cookieSecret: 'kyrgyz-horse',
  onConnect: process.env.CREATE_TABLES !== 'true' && initialiseData,
});

// Access control functions
const userIsAdmin = ({
  authentication: {
    item: user
  }
}) => Boolean(user && user.isAdmin);
const userOwnsItem = ({
  authentication: {
    item: user
  }
}) => {
  if (!user) {
    return false;
  }

  // Instead of a boolean, you can return a GraphQL query:
  // https://www.keystonejs.com/api/access-control#graphqlwhere
  return {
    id: user.id
  };
};

const userIsAdminOrOwner = auth => {
  const isAdmin = access.userIsAdmin(auth);
  const isOwner = access.userOwnsItem(auth);
  return isAdmin ? isAdmin : isOwner;
};

const access = {
  userIsAdmin,
  userOwnsItem,
  userIsAdminOrOwner
};

keystone.createList('User', {
  fields: {
    name: {
      type: Text
    },
    email: {
      type: Text,
      isUnique: true,
    },
    isAdmin: {
      type: Checkbox,
      // Field-level access controls
      // Here, we set more restrictive field access so a non-admin cannot make themselves admin.
      access: {
        update: access.userIsAdmin,
      },
    },
    password: {
      type: Password,
    },
  },
  // List-level access controls
  access: {
    read: access.userIsAdminOrOwner,
    update: access.userIsAdminOrOwner,
    create: access.userIsAdmin,
    delete: access.userIsAdmin,
    auth: true,
  },
});

keystone.createList('Owner', {
  fields: {
    name: {
      type: Text,
      label: 'ФИО',
    },
    horses: {
      label: 'Лошади',
      type: Relationship,
      ref: 'Horse.owner',
      many: true
    },
    birthDate: {
      label: 'Дата рождения',
      type: DateTime,
      format: 'dd.MM.yyyy',
    },
    occupation: {
      label: 'Род деятельности',
      type: Select,
      dataType: 'string',
      options: [
        'Саяпкер',
        'Животновод',
        'Коневод',
        'КРС',
        'Лекарь'
      ]
    },
    occupationBroad: {
      label: 'Род деятельности (Подробно)',
      type: Text,
    },
    address: {
      type: Text,
      label: 'Адрес (Текстовый формат)'
    },
    phoneNumber: {
      type: Text,
      label: 'Номер телефона'
    },
    phoneNumberEmergency: {
      type: Text,
      label: 'Контакт доверенного лица'
    },
    nameEmergency: {
      type: Text,
      label: 'Имя доверенного лица'
    }
  }
})

keystone.createList('Horse', {
  fields: {
    name: {
      type: Text,
      label: 'Кличка лошади'
    },
    age: {
      type: Integer,
      label: 'Возраст (в годах)'
    },
    genderAgeGroup: {
      type: Text,
      label: 'Половозрастрая группа',
    },
    owner: {
      type: Relationship,
      ref: 'Owner.horses',
      label: 'Владелец лошади',
      many: false,
    },
    location: {
      type: Text,
      label: 'Место дислокации'
    },
    comments: {
      type: Text,
      label: 'Примечание'
    },
    measurement1: {
      type: Text,
      label: 'Высота в холке',
    },
    measurement2: {
      type: Text,
      label: 'Обхват, ширина груди',
    },
    measurement3: {
      type: Text,
      label: 'Косая длина туловища',
    },
    measurement4: {
      type: Text,
      label: 'Обхват пясти',
    },
    measurement5: {
      type: Text,
      label: 'Обхват головы',
    },
    measurement6: {
      type: Text,
      label: 'Обхват шеи',
    },
  }
})

const authStrategy = keystone.createAuthStrategy({
  type: PasswordAuthStrategy,
  list: 'User',
});

module.exports = {
  keystone,
  apps: [
    new GraphQLApp(),
    new AdminUIApp({
      name: PROJECT_NAME,
      enableDefaultRoute: true,
      authStrategy,
    }),
  ],
};